import fontSize from '@/tailwindcss/'
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './vueform.config.js',
    './node_modules/@vueform/vueform/themes/tailwind/**/*.vue',
    './node_modules/@vueform/vueform/themes/tailwind/**/*.js'
  ],
  theme: {
    extend: {
      form: theme => ({
        primary: '#07BF9B'
      }),
      screens: {
        'sm-h': '420px',
        'sm-l': '512px',
        sm: '640px',
        'md-h': '680px',
        'md-l': '720px',
        md: '768px',
        'lg-h': '820px',
        'lg-l': '920px',
        lg: '1024px',
        'xl-h': '1140px',
        'xl-l': '1200px',
        xl: '1280px',
        '2xl': '1536px'
      }
    }
  },
  plugins: [
    require('@vueform/vueform/tailwind')
  ]
}
