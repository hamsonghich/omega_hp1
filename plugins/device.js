import Vue from 'vue'
import device from 'vue-device-detector'
if (process.client) {
  Vue.use(device)
}
